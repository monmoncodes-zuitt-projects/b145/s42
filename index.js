
// A document
console.log(document);

// Targeting the first name input field
// document.querySelector("#txt-first-name");

// alternatively:
// document.getElementById("txt-first-name");

// for other types
// document.getElementByClassName('className')
// document.getElementByTagName('tagName')

// Contain the query selector code in a constant

// const txtFirstName = document.querySelector("#txt-first-name");

// const txtLastName = document.querySelector("#txt-first-name");

// const spanFullName = document.querySelector("#span-full-name");

// // Event Listeners
// txtFirstName.addEventListener("keyup", (e) => {
// 	spanFullName.innerHTML = txtFirstName.value;
// });

// // Multiple Listeners
// // "e" is a shorthand for "event"
// txtFirstName.addEventListener("keyup", (e) => {
// 	console.log(e.target);
// 	console.log(e.target.value);
// });

// ACTIVITY
const txtFirstName = document.querySelector("#txt-first-name");

const txtLastName = document.querySelector("#txt-last-name");

let spanFullName = document.querySelector("#span-full-name");

function fullName() {
	txtFirstName.addEventListener("keyup", (e) => {
		spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
	});
	txtLastName.addEventListener("keyup", (e) => {
		spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
	});
}

fullName();
